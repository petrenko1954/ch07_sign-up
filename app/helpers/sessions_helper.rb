#Листинг 8.12: Метод log_in. app/helpers/sessions_helper.rb Листинг 8.14: Поиск текущего #пользователя в сессии. app/helpers/sessions_helper.rbЛистинг 8.26: Метод log_out. app/helpers/#sessions_helper.rb Листинг 8.35: Запоминание пользователя. app/helpers/sessions_helper.rbЛистинг 8.39: Выход из постоянной сессии. app/helpers/sessions_helper.rbЛистинг 8.57: Удаление вызова исключения. ЗЕЛЕНЫЙ app/helpers/sessions_helper.rb    

module SessionsHelper

  # Осуществляет вход данного пользователя.
  def log_in(user)
    session[:user_id] = user.id
  end
# Запоминает пользователя в постоянную сессию.
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end




# Возвращает пользователя, соответствующего remember-токену в куки.
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
# Возвращает true, если пользователь вошел, иначе false.
  def logged_in?
    !current_user.nil?
  end

 # Забывает постоянную сессии.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
 # Осуществляет выход текущего пользователя.
  def log_out
forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

end

